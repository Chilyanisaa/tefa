package com.example.tefa.ui.profile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.tefa.R
import com.example.tefa.base.BaseFragment
import com.example.tefa.databinding.FragmentProfileBinding
import com.example.tefa.ui.home.HomeFragment
import pub.devrel.easypermissions.EasyPermissions


class ProfileFragment : BaseFragment() {
    private lateinit var binding: FragmentProfileBinding
    private var uriFile: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        initView()
        return binding.root
    }

    private fun initView() {
        val perms = Manifest.permission.READ_EXTERNAL_STORAGE
        if (EasyPermissions.hasPermissions(requireContext(), perms)) {
            binding.ivAvatar.setOnClickListener { chooseImageGallery() }
        } else {
            EasyPermissions.requestPermissions(
                this, getString(R.string.permission_rationale_gallery),
                PERMISSION_CODE, perms
            )
        }
    }

    private fun chooseImageGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, IMAGE_CHOOSE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IMAGE_CHOOSE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                uriFile = data.data
                binding.ivAvatar.setImageURI(data.data)
            }
        }
    }

    companion object {
        private const val PERMISSION_CODE = 1001
        private const val IMAGE_CHOOSE = 1000
    }
}